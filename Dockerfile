FROM php:8.1-fpm

RUN rm /bin/sh && ln -s /bin/bash /bin/sh

COPY composer.lock composer.json /var/www/
COPY package-lock.json package.json /var/www/
WORKDIR /var/www

RUN apt-get update && apt-get install -y \
  libzip-dev \
  libonig-dev \
  build-essential \
  libpng-dev \
  libmagickwand-dev \
  libjpeg62-turbo-dev \
  libfreetype6-dev \
  locales \
  zip \
  jpegoptim optipng pngquant gifsicle \
  vim \
  unzip \
  git \
  curl

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install pdo_mysql mbstring zip exif pcntl
RUN apt-get update && apt-get install -y \
       libfreetype6-dev \
       libjpeg62-turbo-dev \
       libpng-dev \
   && docker-php-ext-configure gd --with-freetype --with-jpeg \
   && docker-php-ext-install -j$(nproc) gd

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Node.js
RUN curl -sL https://deb.nodesource.com/setup_16.x -o nodesource_setup.sh
RUN bash nodesource_setup.sh
RUN apt-get install nodejs -y
RUN npm install -g npm@8.5.5
RUN command -v node
RUN command -v npm

COPY . /var/www

# Display versions installed
RUN php -v
RUN composer --version
RUN node -v
RUN npm -v

EXPOSE 9000
CMD ["php-fpm"]
