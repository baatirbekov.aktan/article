<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Like;
use App\Models\User;
use Illuminate\Database\Seeder;

class LikeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $user = User::factory()->create([
            'email' => 'admin@admin.com'
        ]);

        $articles = Article::all();

        foreach ($articles as $article) {
            Like::factory()->for($users->random())->for($article)->create();
            Like::factory()->for($user)->for($article)->create();
        }

    }
}
