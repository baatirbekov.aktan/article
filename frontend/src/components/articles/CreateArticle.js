import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import {useState, useEffect} from "react";
import {post, get} from '../../request';
import {isAuth} from "../../utils";
import LoadingButton from "@mui/lab/LoadingButton";
import InputLabel from '@mui/material/InputLabel';
import Select, {SelectChangeEvent} from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';

const CreateArticle = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [article, setDataArticle] = useState({title: '', content: '', category_id: ''});
    const [categories, setCategories] = useState(null)

    const token = isAuth();

    useEffect(() => {
        if (!token) {
            window.location.href = '/';
        }
        setIsLoading(true);
        const getCategories = get("/categories", token);
        getCategories.then(payload => {
            setCategories(payload.data);
            setIsLoading(false);
        })

    }, []);

    const handleChange = (event: SelectChangeEvent) => {
        setDataArticle({
            ...article, [event.target.name]: event.target.value
        });
    };

    const handleArticleForm = (event) => {
        setDataArticle({
            ...article, [event.target.name]: event.target.value
        });
    }

    const handleSubmit = (event) => {
        setIsLoading(true);
        event.preventDefault();

        console.log(article)

        const response = post("/articles", article, token);
        response.then(payload => {
            if (Object.keys(payload).includes('data')) {
                window.location.replace("/articles");
            } else {
                console.log(payload);
            }
            setIsLoading(false);
        });

    };

    return (<Box
        component="form"
        sx={{
            '& .MuiTextField-root': {m: 1, width: '100%'},
        }}
        autoComplete="off"
        onSubmit={handleSubmit}
    >
        <h1>Create new article</h1>

        <InputLabel sx={{m: 1}} id="category">Category</InputLabel>
        {categories && <Select
            labelId="category"
            value={article.category_id}
            label="Category"
            name="category_id"
            sx={{m: 1}}
            fullWidth
            onChange={handleChange}
        >
            {categories.map((category, key) => ([
                    <MenuItem key={key} value={category.id}>{category.title}</MenuItem>
                ]
            ))
            }
        < /Select>}
        <TextField
            label="Title"
            required
            id="outlined-size-small"
            value={article.title}
            size="small"
            fullWidth
            name="title"
            onChange={handleArticleForm}
        />
        <TextField
            id="outlined-multiline-static"
            label="Content"
            multiline
            required
            fullWidth
            rows={4}
            name="content"
            value={article.content}
            onChange={handleArticleForm}
        />

        <LoadingButton
            type="submit"
            fullWidth
            variant="contained"
            sx={{mt: 3, mb: 2}}
            loading={isLoading}
        >
            Create
        </LoadingButton>
    </Box>);
}

export default CreateArticle;
