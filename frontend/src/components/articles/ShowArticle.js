import * as React from 'react';
import Box from '@mui/material/Box';
import {useState, useEffect} from "react";
import LinearProgress from "@mui/material/LinearProgress";
import Stack from '@mui/material/Stack';
import {Link as RouterLink} from "react-router-dom";
import Button from '@mui/material/Button';
import { get } from '../../request';
import {isAuth} from "../../utils";
import { useParams } from 'react-router-dom';

const ShowArticle = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [article, setArticle] = useState(null);
    const { articleId } = useParams();
    const token = isAuth();

    useEffect(() => {
        if (!token) {
            window.location.href = '/';
        } else {
            setIsLoading(true);
            const response = get(`/articles/${articleId}`, token);
            response.then(res => {
                if (Object.keys(res).includes('data')) {
                    setArticle(res.data);
                } else {
                    console.log(res);
                }
                setIsLoading(false);
            });
        }
    }, []);

    return (
        <>
            {isLoading &&
            <Box sx={{width: '100%'}}>
                <LinearProgress/>
            </Box>
            }
            {article &&
            <Box sx={{width: '100%'}}>
                <h1>{article.title}</h1>
                <p>
                    {article.content}
                </p>
                <p>
                    Author: {article.user.name}
                </p>
                <Stack spacing={2} direction="row">
                    <Button component={RouterLink} to={'/articles/'} variant="contained">Back</Button>
                    <Button component={RouterLink} to={`/articles/edit/${articleId}`} variant="outlined">Edit</Button>
                </Stack>
            </Box>
            }
        </>
    );
};
export default ShowArticle;
