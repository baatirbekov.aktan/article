export function isAuth() {
    return localStorage.getItem('token') || false;
}

export function AuthId(){
    return localStorage.getItem('id') || false;
}

